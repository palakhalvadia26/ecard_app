angular.module('starter.controllers')
.controller('searchCtrl', function ($ionicFilterBar) {
var vm = this;
vm.datashow="data" ;
vm.showFilterBar = function () {
  var filterBarInstance = $ionicFilterBar.show({
    cancelText: "<i class='ion-ios-close-outline'></i>"
    // items: vm.places,
    // update: function (filteredItems, filterText) {
    //   vm.places = filteredItems;
    // }
  });
};
// vm.places = [{name:'New York'}, {name: 'London'}, {name: 'Milan'}, {name:'Paris'}];
vm.profiles =
  [{
    //img ng-src="http://ionicframework.com/img/docs/mcfly.jpg",
    thumbnailPath: "http://ionicframework.com/img/docs/mcfly.jpg",
    name: 'John',
    designation: 'Software Developer'},
   {thumbnailPath: "http://ionicframework.com/img/docs/mcfly.jpg",
    name: 'Sam',
    designation: 'QA'},
   {thumbnailPath: "http://ionicframework.com/img/docs/mcfly.jpg",
    name: 'David',
    designation: 'BA'},
   {thumbnailPath: "http://ionicframework.com/img/docs/mcfly.jpg",
    name: 'Ron',
    designation: 'HR'},
   {thumbnailPath: "http://ionicframework.com/img/docs/mcfly.jpg",
    name: 'Allen',
    designation: 'Software Developer'},
   {thumbnailPath: "http://ionicframework.com/img/docs/mcfly.jpg",
    name: 'Shawn',
    designation: 'Director'}
];
})
